/*
 * Author: N.Kovalenko
 * StartDate: 28.11/2017
 * EndDate: 28.11/2017
 */

(function () {
    'use strict';

    var Company = window.Company || {};


    Company = {

        init: function () {
            try {
                this.newsModuleOnLoad();
                this.addPhonesToHeader();
            } catch (err) {
                console.log(err);
            }
        },

        /*
         * data-module="newsModuleOnLoad"
         * Public Function For Add News TO data-module="newsModuleOnLoad"
         * */

        newsModuleOnLoad: function () {
            var url = "api/get_news/news.json",
                that = this,
                errorText = "Sorry, can`t to load news, check your server settings or url";


            /* Private Func for Append News */
            function appendNews(response) {
                $('[data-module="newsModuleOnLoad"]').append(
                    '<div class="newsModule__item">' +
                    '<div class="newsModule__item-header">' +
                    '<div class="date">'
                    + response.date +
                    '</div>' +
                    '<h3><a href="' + response.url + '">' + response.title + '</a></h3>' +
                    '</div>' +
                    '</div>'
                );

                return true;
            }

            /* Private Func for Ajax Request To Get News API.
             * URL - api/get_news/news.json
             * */

            $.ajax({
                type: "GET",
                dataType: "JSON",
                url: url,
                success: function (response) {

                    if (response.succses === true) {

                        /* Add News */
                        for (var i = 0; i < response.news.length; i++) {
                            appendNews(response.news[i]);
                        }

                        /* Set Status - News Added*/
                        $('[data-module="newsModuleOnLoad"]').attr('data-moduleLoadStatus', 'true');
                    }

                    return true;

                },
                error: function () {
                    $('[data-module="newsModuleOnLoad"]').append('<p>' + errorText + '</p>').attr('data-moduleLoadStatus', 'true');
                    $('.read__more').hide();
                }
            });
        },


        /*
         * data-module="addPhonesToHeader"
         * API URL = https://freegeoip.net/json/
         * Public Function For Add Phones TO data-module="addPhonesToHeader"
         * */
        addPhonesToHeader: function () {

            var urlTarget = "https://freegeoip.net/json/", country = null;

            /* Private Func TO set Phones By Country */
            function setPhones(country) {
                var phones = null;
                switch (country) {
                    case "Ukraine":
                        phones = [
                            "+380 66 639 87 45",
                            "+380 66 639 99 45"
                        ];
                        break;
                    case "Russia":
                        phones = [
                            "+7(499) 777 77 77",
                            "+7(499) 777 77 77"
                        ];
                        break;
                    case "All":
                        phones = [
                            "+380 66 639 87 45",
                            "+7(499) 777 77 77"
                        ];
                        break;
                }

                return phones;
            }

            /* Private Func To add Phones To Header */

            function appendPhones(country) {
                var phones = setPhones(country);

                for (var i = 0; i < phones.length; i++) {
                    $('[data-module="addPhonesToHeader"]').append(
                        '<li><a href="tel:' + phones[i] + '">' + phones[i] + '</a></li>'
                    );
                }

                return true;

            }

            /* Get User Location from API URL */
            $.ajax({
                type: "GET",
                dataType: "JSON",
                url: urlTarget,
                success: function (response) {

                    /* get country */
                    if (response.country_name !== null && typeof response.country_name !== 'undefined') {
                        country = response.country_name;
                    } else {
                        country = "All"
                    }

                    /* append country list to header  */
                    appendPhones(country);

                    /* set module load status */
                    $('[data-module="newsModuleOnLoad"]').attr('data-moduleLoadStatus', 'true');

                },
                error: function () {
                    /* get country */
                    country = "All";

                    /* append country list to header  */
                    appendPhones(country);

                    /* set module load status */
                    $('[data-module="newsModuleOnLoad"]').attr('data-moduleLoadStatus', 'true');

                }

            });

        }

    };

    /*
    * Init Object
    * */

    $(document).ready(function () {
        Company.init();

        if (window.matchMedia("(min-width: 1150px)").matches) {
            $('.slider__wrapper').slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 1,
                adaptiveHeight: false,
                arrows: false
                //variableWidth: true,
                //centerMode: true
            });
        }

    });


})();
